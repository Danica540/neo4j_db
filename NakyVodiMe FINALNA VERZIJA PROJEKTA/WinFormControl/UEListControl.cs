﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4j.Models;

namespace NakyVodiMe.WinFormControler
{
    
    public partial class UEListControl : UserControl
    {
        string flag;

        public UEListControl()
        {
            InitializeComponent();
        }

        

        private void ShowUsers(IEnumerable<UserNod> users)
        {
            flowLayoutPanel1.Controls.Clear();

            foreach(UserNod u in users)
            {
                
                SingleUserItemControl sui = new SingleUserItemControl(u.Username, this);
                flowLayoutPanel1.Controls.Add(sui);
            }
        }

        public void setUsers(String tip)
        {
            if (tip == "Followers")
            {
                ShowUsers(MainForm.ru.Followers);
                flag = "Followers";
            }
            else if (tip == "Following")
            {
                ShowUsers(MainForm.ru.Following);
                flag = "Following";
            }
        }

        public void setEvents(String tip)
        {
            if (tip == "Created")
            {
                ShowEvents(MainForm.ru.Created);
                flag = "Created";
            }
            else if (tip == "GoingTo")
            {
                ShowEvents(MainForm.ru.GoingTo);
                flag = "GoingTo";
            }
        }

        public void setEvents(IEnumerable<EventNod> events)
        {
            ShowEvents(events);
            flag = "NE";
        }

       public void setUsers(IEnumerable<UserNod> users)
        {
            ShowUsers(users);
            flag = "NE";
        }

        public void RefreshData()
        {
            if (flag == "Following" )
            {
                ShowUsers(MainForm.ru.Following);
            }
            else if(flag == "Followers")
            {
                ShowUsers(MainForm.ru.Followers);
            }
            else if (flag == "Created") 
            {
                ShowEvents(MainForm.ru.Created);
            }
            else if(flag == "GoingTo")
            {
                ShowEvents(MainForm.ru.GoingTo);
            }
        }


        private void ShowEvents(IEnumerable<EventNod> events)
        {
            flowLayoutPanel1.Controls.Clear();

            foreach (EventNod e in events)
            {

                SingleEventItemControl sei = new SingleEventItemControl( e.id, e.Title, this);
                flowLayoutPanel1.Controls.Add(sei);
            }
        }

    }
}
