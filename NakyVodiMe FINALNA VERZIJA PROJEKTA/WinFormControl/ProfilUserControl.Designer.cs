﻿namespace NakyVodiMe
{
    partial class ProfilUserControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblName = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.btnFollowers = new System.Windows.Forms.Button();
            this.btnFollowing = new System.Windows.Forms.Button();
            this.btnCreated = new System.Windows.Forms.Button();
            this.btnGoing = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnEdit = new System.Windows.Forms.Button();
            this.btnAddEvent = new System.Windows.Forms.Button();
            this.userListControl1 = new NakyVodiMe.WinFormControler.UEListControl();
            this.SuspendLayout();
            // 
            // lblName
            // 
            this.lblName.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 28F);
            this.lblName.Location = new System.Drawing.Point(12, 15);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(147, 54);
            this.lblName.TabIndex = 0;
            this.lblName.Text = "label1";
            // 
            // lblUsername
            // 
            this.lblUsername.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblUsername.AutoSize = true;
            this.lblUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.lblUsername.Location = new System.Drawing.Point(16, 69);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(81, 29);
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "label2";
            // 
            // btnFollowers
            // 
            this.btnFollowers.Location = new System.Drawing.Point(21, 117);
            this.btnFollowers.Name = "btnFollowers";
            this.btnFollowers.Size = new System.Drawing.Size(176, 48);
            this.btnFollowers.TabIndex = 2;
            this.btnFollowers.Text = "FOLLOWERS";
            this.btnFollowers.UseVisualStyleBackColor = true;
            this.btnFollowers.Click += new System.EventHandler(this.btnFollowers_Click);
            // 
            // btnFollowing
            // 
            this.btnFollowing.Location = new System.Drawing.Point(216, 117);
            this.btnFollowing.Name = "btnFollowing";
            this.btnFollowing.Size = new System.Drawing.Size(176, 48);
            this.btnFollowing.TabIndex = 3;
            this.btnFollowing.Text = "FOLLOWING";
            this.btnFollowing.UseVisualStyleBackColor = true;
            this.btnFollowing.Click += new System.EventHandler(this.btnFollowing_Click);
            // 
            // btnCreated
            // 
            this.btnCreated.Location = new System.Drawing.Point(414, 117);
            this.btnCreated.Name = "btnCreated";
            this.btnCreated.Size = new System.Drawing.Size(176, 48);
            this.btnCreated.TabIndex = 4;
            this.btnCreated.Text = "CREATED EVENTS";
            this.btnCreated.UseVisualStyleBackColor = true;
            this.btnCreated.VisibleChanged += new System.EventHandler(this.btnCreated_VisibleChanged);
            this.btnCreated.Click += new System.EventHandler(this.btnCreated_Click);
            // 
            // btnGoing
            // 
            this.btnGoing.Location = new System.Drawing.Point(606, 117);
            this.btnGoing.Name = "btnGoing";
            this.btnGoing.Size = new System.Drawing.Size(176, 48);
            this.btnGoing.TabIndex = 5;
            this.btnGoing.Text = "GOING TO EVENTS";
            this.btnGoing.UseVisualStyleBackColor = true;
            this.btnGoing.Click += new System.EventHandler(this.btnGoing_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(604, 10);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(178, 35);
            this.btnDelete.TabIndex = 7;
            this.btnDelete.Text = "DELETE PROFIL";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnEdit
            // 
            this.btnEdit.Location = new System.Drawing.Point(604, 62);
            this.btnEdit.Name = "btnEdit";
            this.btnEdit.Size = new System.Drawing.Size(178, 36);
            this.btnEdit.TabIndex = 8;
            this.btnEdit.Text = "EDIT PROFIL";
            this.btnEdit.UseVisualStyleBackColor = true;
            this.btnEdit.Click += new System.EventHandler(this.btnEdit_Click);
            // 
            // btnAddEvent
            // 
            this.btnAddEvent.Location = new System.Drawing.Point(495, 10);
            this.btnAddEvent.Name = "btnAddEvent";
            this.btnAddEvent.Size = new System.Drawing.Size(95, 88);
            this.btnAddEvent.TabIndex = 9;
            this.btnAddEvent.Text = "ADD EVENT";
            this.btnAddEvent.UseVisualStyleBackColor = true;
            this.btnAddEvent.Click += new System.EventHandler(this.btnAddEvent_Click);
            // 
            // userListControl1
            // 
            this.userListControl1.Location = new System.Drawing.Point(21, 171);
            this.userListControl1.Name = "userListControl1";
            this.userListControl1.Size = new System.Drawing.Size(761, 324);
            this.userListControl1.TabIndex = 6;
            // 
            // ProfilUserControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnAddEvent);
            this.Controls.Add(this.btnEdit);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.userListControl1);
            this.Controls.Add(this.btnGoing);
            this.Controls.Add(this.btnCreated);
            this.Controls.Add(this.btnFollowing);
            this.Controls.Add(this.btnFollowers);
            this.Controls.Add(this.lblUsername);
            this.Controls.Add(this.lblName);
            this.Name = "ProfilUserControl";
            this.Size = new System.Drawing.Size(808, 541);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Button btnFollowers;
        private System.Windows.Forms.Button btnFollowing;
        private System.Windows.Forms.Button btnCreated;
        private System.Windows.Forms.Button btnGoing;
        private WinFormControler.UEListControl userListControl1;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnEdit;
        private System.Windows.Forms.Button btnAddEvent;
    }
}
