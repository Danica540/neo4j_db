﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4j.Providers;
using Neo4j.Models;

namespace NakyVodiMe.WinFormControler
{
    public partial class SingleEventItemControl : UserControl
    {
        private RecEvent re;
        private UEListControl ulc;
        private int id;
      
        public SingleEventItemControl()
        {
            InitializeComponent();
        }

        public SingleEventItemControl(int i,String title, UEListControl uc)
        {
            InitializeComponent();
            ulc = uc;
            id = i;
            label2.Text = title;
         
        }

        private void btnOpen_Click(object sender, EventArgs e)
        {
            EventForm ef = new EventForm(id, ulc);
            ef.ShowDialog();

        }

       
    }
}
