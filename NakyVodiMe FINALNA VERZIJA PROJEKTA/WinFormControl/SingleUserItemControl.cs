﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe.WinFormControler
{
    public partial class SingleUserItemControl : UserControl
    {
        private String Username;
        private UEListControl ulc;

        public SingleUserItemControl()
        {
            InitializeComponent();
        }

        public SingleUserItemControl(String un, UEListControl uc)
        {
            InitializeComponent();
            Username = un;
            label1.Text = un;
            ulc = uc;
        }


        private void btnOpen_Click(object sender, EventArgs e)
        {
            UserForm uf = new UserForm(Username, ulc);
            uf.Show();

        }
    }
}
