﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4j.Providers;
using Neo4j.Models;

namespace NakyVodiMe.WinFormControl
{

    public enum Grad
    {
        Nis = 0,
        Beograd = 1,
        NoviSad = 2,
        Kragujevac = 3,
        Leskovac = 4,
        Prokuplje = 5,
        Pirot = 6
    }

    public enum Tip
    {
        Zurka = 0,
        Karaoke = 1,
        Degustacija = 2,
        KnjizevnoVece = 3,
        Izlozba = 4,
        Sajam = 5,
        Predstava = 6
    }

    public partial class EventsControl : UserControl
    {
        EvProvider evProvider;
        public EventsControl()
        {
            InitializeComponent();
            cbTip.DataSource = Enum.GetValues(typeof(Tip));
            cbGrad.DataSource = Enum.GetValues(typeof(Grad));
           

        }

        public void showTrending()
        {
            evProvider = EvProvider.GetEvProvider();
            List<EventNod> join = new List<EventNod>();
            join.AddRange(evProvider.GetRecommendedFromEvents(MainForm.ru.user.Username));
            join.AddRange(evProvider.GetRecommendedFromFriends(MainForm.ru.user.Username));
            elTrending.setEvents(join);
        }

        private void btnSearch_Click(object sender, EventArgs e)
        {
            string grad, tip;
            grad = cbGrad.Text;
            tip = cbTip.Text;
            List<EventNod> join = new List<EventNod>();
            evProvider = EvProvider.GetEvProvider();
            join.AddRange(evProvider.GetEvents(grad, tip));
            elSearch.setEvents(join);
        }
    }
}
