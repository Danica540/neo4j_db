﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4j.Providers;
using Neo4j.Models;

namespace NakyVodiMe.WinFormControl
{
    public partial class UsersControl : UserControl
    {
        public UsersControl()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            UserProvider up = UserProvider.GetUserProvider();
            List<UserNod> join = new List<UserNod>();
            join.AddRange(up.FindUsers(tbSearch.Text.ToLower()));
            ulcSearch.setUsers(join);
        }

        public void ShowTopUsers()
        {
            UserProvider up = UserProvider.GetUserProvider();
            List<UserNod> join = new List<UserNod>();
            join.AddRange(up.TopUsers());
            join.AddRange(up.GetRecommended(MainForm.ru.user.Username));
            ulcRec.setUsers(join);
        }
    }
}
