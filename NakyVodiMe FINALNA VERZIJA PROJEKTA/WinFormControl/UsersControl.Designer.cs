﻿namespace NakyVodiMe.WinFormControl
{
    partial class UsersControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ulcRec = new NakyVodiMe.WinFormControler.UEListControl();
            this.ulcSearch = new NakyVodiMe.WinFormControler.UEListControl();
            this.button1 = new System.Windows.Forms.Button();
            this.tbSearch = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ulcRec
            // 
            this.ulcRec.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ulcRec.Location = new System.Drawing.Point(32, 34);
            this.ulcRec.Name = "ulcRec";
            this.ulcRec.Size = new System.Drawing.Size(745, 220);
            this.ulcRec.TabIndex = 0;
            // 
            // ulcSearch
            // 
            this.ulcSearch.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.ulcSearch.Location = new System.Drawing.Point(32, 306);
            this.ulcSearch.Name = "ulcSearch";
            this.ulcSearch.Size = new System.Drawing.Size(745, 232);
            this.ulcSearch.TabIndex = 1;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(32, 270);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(209, 30);
            this.button1.TabIndex = 2;
            this.button1.Text = "SEARCH";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tbSearch
            // 
            this.tbSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSearch.Location = new System.Drawing.Point(247, 270);
            this.tbSearch.Name = "tbSearch";
            this.tbSearch.Size = new System.Drawing.Size(530, 30);
            this.tbSearch.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(273, 26);
            this.label1.TabIndex = 4;
            this.label1.Text = "RECOMMENDED USERS";
            // 
            // UsersControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbSearch);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.ulcSearch);
            this.Controls.Add(this.ulcRec);
            this.Name = "UsersControl";
            this.Size = new System.Drawing.Size(808, 541);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private WinFormControler.UEListControl ulcRec;
        private WinFormControler.UEListControl ulcSearch;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox tbSearch;
        private System.Windows.Forms.Label label1;
    }
}
