﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Neo4j.Models;
using Neo4j.Providers;

namespace NakyVodiMe
{
    public partial class ProfilUserControl : UserControl
    {
        string username;
        static Form parent;
        public ProfilUserControl()
        {
            InitializeComponent();
        }
               
        public void getUser(RecUser ru)
        {
            
            ShowUsers("Followers");
            username = ru.user.Username;
            updatelabel(ru.user.Name, ru.user.Lastname, ru.user.Username);
           
        }

        public void ShowUsers(String tip)
        {
            userListControl1.setUsers(tip);
        }



        private void btnFollowing_Click(object sender, EventArgs e)
        {

            userListControl1.setUsers("Following");
           
        }

        private void btnFollowers_Click(object sender, EventArgs e)
        {
            ShowUsers("Followers");
      
        }

        private void btnCreated_Click(object sender, EventArgs e)
        {
            userListControl1.setEvents("Created");
            
        }

        private void btnGoing_Click(object sender, EventArgs e)
        {
            userListControl1.setEvents("GoingTo");
            
        }

        public void setForm(Form f)
        {
            parent = f;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            UserProvider up = UserProvider.GetUserProvider();
            up.DeleteUser(username);
            parent.Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditUser eu = new EditUser(MainForm.ru.user.Username, this);
            eu.Show();
        }

        public void updatelabel(String name, String lastname, String username)
        {
            lblName.Text = name + " " + lastname;
            lblUsername.Text = username;
        }

        private void btnAddEvent_Click(object sender, EventArgs e)
        {
            AddEvent ae = new AddEvent(this);
            ae.Show();
        }

        public void refreshEvents(String tipS)
        {
            userListControl1.setEvents(tipS);
        }

        private void btnCreated_VisibleChanged(object sender, EventArgs e)
        {
            userListControl1.RefreshData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            LogInSignUp logInSignUp = new LogInSignUp();
            logInSignUp.Show();
            this.Hide();
        }
    }
}
