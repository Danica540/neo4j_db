﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe
{
    public partial class LogInSignUp : Form
    {

        private RecUser ru;
        private UserProvider userProvider;

        public LogInSignUp()
        {
            InitializeComponent();
            ru = new RecUser();
            userProvider = UserProvider.GetUserProvider();
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void LogInSignUp_Load(object sender, EventArgs e)
        {

        }

        private void btnLogIn_Click(object sender, EventArgs e)
        {

            ru = userProvider.LogIn(tbLogInUsername.Text, tbLogInPass.Text);
            if (ru == null || ru.user==null)
                MessageBox.Show("Bad username or password");
            else
            {
                this.Hide();
                MainForm mf = new MainForm(ru);
                mf.ShowDialog();
                this.Close();
            }

        }

        private void btnSignUp_Click(object sender, EventArgs e)
        {
            if(tbSignUPPass.Text!=tbSignUpPass2.Text && tbSignUPPass.Text=="")
            {
                MessageBox.Show("password must match");
            }
            else if (UserProvider.GetUserProvider().GetUser(tbSignUpUsername.Text) != null && tbSignUpUsername.Text!="")
            {
                MessageBox.Show("Username is taken");
            }
            else
            {
                User u = new User();
                u.Name = tbSignUpName.Text;
                u.Lastname = tbSignUpLastname.Text;
                u.Password = tbSignUPPass.Text;
                u.Username = tbSignUpUsername.Text;
                u.Searchname = tbSignUpUsername.Text.ToLower();

                ru = userProvider.RegisterUser(u);
                if (ru.user == null)
                    MessageBox.Show("error in registration");
                else
                {
                    this.Hide();
                    MainForm mf = new MainForm(ru);
                    mf.ShowDialog();
                    this.Close();
                }
               
                this.Close();
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            userProvider.PopulateDataBase();
            EvProvider.GetEvProvider().PopulateDataBase();
        }
    }
}
