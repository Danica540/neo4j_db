﻿namespace NakyVodiMe
{
    partial class UserForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.N = new System.Windows.Forms.Label();
            this.ltname = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.lbGoing = new System.Windows.Forms.Label();
            this.lbCreated = new System.Windows.Forms.Label();
            this.lbFollowing = new System.Windows.Forms.Label();
            this.lbFollowers = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbLastname = new System.Windows.Forms.Label();
            this.lbName = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // N
            // 
            this.N.AutoSize = true;
            this.N.Location = new System.Drawing.Point(38, 84);
            this.N.Name = "N";
            this.N.Size = new System.Drawing.Size(49, 17);
            this.N.TabIndex = 0;
            this.N.Text = "Name:";
            // 
            // ltname
            // 
            this.ltname.AutoSize = true;
            this.ltname.Location = new System.Drawing.Point(38, 120);
            this.ltname.Name = "ltname";
            this.ltname.Size = new System.Drawing.Size(74, 17);
            this.ltname.TabIndex = 1;
            this.ltname.Text = "Lastname:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(38, 156);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 2;
            this.label3.Text = "Username:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(39, 192);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(71, 17);
            this.label4.TabIndex = 3;
            this.label4.Text = "Followers:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(38, 231);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(70, 17);
            this.label5.TabIndex = 4;
            this.label5.Text = "Following:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(38, 271);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(62, 17);
            this.label6.TabIndex = 5;
            this.label6.Text = "Created:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(38, 308);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 17);
            this.label7.TabIndex = 6;
            this.label7.Text = "Going:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(120, 378);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(144, 80);
            this.button1.TabIndex = 7;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // lbGoing
            // 
            this.lbGoing.AutoSize = true;
            this.lbGoing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbGoing.Location = new System.Drawing.Point(137, 301);
            this.lbGoing.Name = "lbGoing";
            this.lbGoing.Size = new System.Drawing.Size(76, 25);
            this.lbGoing.TabIndex = 14;
            this.lbGoing.Text = "Going:";
            // 
            // lbCreated
            // 
            this.lbCreated.AutoSize = true;
            this.lbCreated.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbCreated.Location = new System.Drawing.Point(137, 264);
            this.lbCreated.Name = "lbCreated";
            this.lbCreated.Size = new System.Drawing.Size(96, 25);
            this.lbCreated.TabIndex = 13;
            this.lbCreated.Text = "Created:";
            // 
            // lbFollowing
            // 
            this.lbFollowing.AutoSize = true;
            this.lbFollowing.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFollowing.Location = new System.Drawing.Point(137, 224);
            this.lbFollowing.Name = "lbFollowing";
            this.lbFollowing.Size = new System.Drawing.Size(110, 25);
            this.lbFollowing.TabIndex = 12;
            this.lbFollowing.Text = "Following:";
            // 
            // lbFollowers
            // 
            this.lbFollowers.AutoSize = true;
            this.lbFollowers.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbFollowers.Location = new System.Drawing.Point(137, 185);
            this.lbFollowers.Name = "lbFollowers";
            this.lbFollowers.Size = new System.Drawing.Size(111, 25);
            this.lbFollowers.TabIndex = 11;
            this.lbFollowers.Text = "Followers:";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsername.Location = new System.Drawing.Point(137, 149);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(117, 25);
            this.lbUsername.TabIndex = 10;
            this.lbUsername.Text = "Username:";
            // 
            // lbLastname
            // 
            this.lbLastname.AutoSize = true;
            this.lbLastname.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLastname.Location = new System.Drawing.Point(137, 113);
            this.lbLastname.Name = "lbLastname";
            this.lbLastname.Size = new System.Drawing.Size(113, 25);
            this.lbLastname.TabIndex = 9;
            this.lbLastname.Text = "Lastname:";
            // 
            // lbName
            // 
            this.lbName.AutoSize = true;
            this.lbName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbName.Location = new System.Drawing.Point(137, 76);
            this.lbName.Name = "lbName";
            this.lbName.Size = new System.Drawing.Size(75, 25);
            this.lbName.TabIndex = 8;
            this.lbName.Text = "Name:";
            // 
            // UserForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 523);
            this.Controls.Add(this.lbGoing);
            this.Controls.Add(this.lbCreated);
            this.Controls.Add(this.lbFollowing);
            this.Controls.Add(this.lbFollowers);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.lbLastname);
            this.Controls.Add(this.lbName);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ltname);
            this.Controls.Add(this.N);
            this.Name = "UserForm";
            this.Text = "UserForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label N;
        private System.Windows.Forms.Label ltname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label lbGoing;
        private System.Windows.Forms.Label lbCreated;
        private System.Windows.Forms.Label lbFollowing;
        private System.Windows.Forms.Label lbFollowers;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbLastname;
        private System.Windows.Forms.Label lbName;
    }
}