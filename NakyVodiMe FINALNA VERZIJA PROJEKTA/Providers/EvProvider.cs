﻿using Neo4j.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Providers
{
    public enum Grad
    {
        Nis = 0,
        Beograd = 1,
        NoviSad = 2,
        Kragujevac = 3,
        Leskovac = 4,
        Prokuplje = 5,
        Pirot = 6
    }

    public enum Tip
    {
        Zurka = 0,
        Karaoke = 1,
        Degustacija = 2,
        KnjizevnoVece = 3,
        Izlozba = 4,
        Sajam = 5,
        Predstava = 6
    }
    public class EvProvider
    {
        private GraphClient client;
        private Dictionary<string, object> queryDict;
        private static EvProvider evProvider;

        public static EvProvider GetEvProvider()
        {
            if(evProvider==null)
            {
                evProvider = new EvProvider();
            }
            return evProvider;
        }

        private EvProvider()
        {
            queryDict = new Dictionary<string, object>();

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "danoi123");
            try
            {
                client.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public RecEvent AddEvent(EventAll e)
        {
         
            int id;
            queryDict.Clear();

            queryDict.Add("Title", e.Title);
            queryDict.Add("Description", e.Description);
            queryDict.Add("Date", e.Date.Date);
            queryDict.Add("Time", e.Time.TimeOfDay);
            queryDict.Add("Place", e.Place);
            queryDict.Add("Address", e.Address);
            queryDict.Add("City", e.Grad);
            queryDict.Add("Type", e.Tip);
            queryDict.Add("Creator", e.Creator);

            var query = new Neo4jClient.Cypher.CypherQuery("MERGE (id:GlobalUniqueId) ON CREATE SET id.count = 100 ON MATCH SET id.count = id.count + 1 RETURN id.count AS generated_id",
                                                          queryDict, CypherResultMode.Set);

            id = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).SingleOrDefault();

            if (id == 0)
            {
                return null;
            }
            queryDict.Add("id", id);


            RecEvent ev = client.Cypher
                .Merge("(tip:Tip{ Naziv:{Type} })").With("tip")
                .Merge("(lok:Lokacija{ Naziv:{City}})").With(" tip, lok")
                .Create("(n:Event { id:{id}, Title:{Title}, Description:{Description}, Date:{Date}, Time:{Time},Place:{Place},Address:{Address}})")
                .With("tip, lok, n")
                .Create("(tip)-[sa:PripadaTip]->(n)<-[sb:PripadaGrad]-(lok)")
                .With("tip, lok, n")
                .Match("(user: User{ Username:{Creator} })")
                .With("tip, lok, n, user")
                .Create("(user) -[p: Creator]->(n)")
                .WithParams(queryDict)
                .Return((n, user, tip, lok) => new RecEvent
                {
                    ev = n.As<EventNod>(),
                    City = lok.As<CityNod>(),
                    Creator = user.As<UserNod>(),
                    Type = tip.As<TypeNod>()
                }).Results.FirstOrDefault();


            queryDict.Clear();
            

            return ev;
        }



        private DateTime RandomDay()
        {
            Random gen = new Random();
            DateTime end = new DateTime(2020, 5, 5);
            int range = (end-DateTime.Today).Days;
            return DateTime.Today.AddDays(gen.Next(range));
        }

        private DateTime RandomTime()
        {
            Random rand = new Random();
            DateTime myDateTime = new DateTime(2012, 11, 27, rand.Next(0, 24), rand.Next(0, 60), 0);
            return myDateTime;
        }


        public void PopulateDataBase()
        {
            Random random = new Random();
            UserProvider userProvider = UserProvider.GetUserProvider();
           
            string[] p = { "NEZABORAVNO", "Meksicko", "Opako", "Ludo", "Zimsko", "Letnje", "Srpsko", "Bajkovito" };
            List<string> pridevi = new List<string>(p);

            List<string> dogadjaji = Enum.GetValues(typeof(Tip)).Cast<Tip>().Select(v => v.ToString()).ToList();

            List<string> gradovi = Enum.GetValues(typeof(Grad)).Cast<Grad>().Select(v => v.ToString()).ToList();

            string[] m = { "Atlantis", "Komuna", "Narodno pozoriste", "Lutkarsko pozoriste", "Pleasure", "Tvrdjava", "Cair", "Trg" };
            List<string> mesto = new List<string>(m);

            List<UserNod> users;

            var queryUser = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User)  RETURN n Limit 50",
                                                            queryDict, CypherResultMode.Set);

            users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(queryUser).ToList();


            for (int i = 0; i < 200; i++)
            {
                queryDict.Clear();
                int id;
                var query = new Neo4jClient.Cypher.CypherQuery("MERGE (id:GlobalUniqueId) ON CREATE SET id.count = 100 ON MATCH SET id.count = id.count + 1 RETURN id.count AS generated_id",
                                                            queryDict, CypherResultMode.Set);

                id = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).SingleOrDefault();

                if (id == 0)
                {
                    continue;
                }

                queryDict.Add("id", id);
                queryDict.Add("Title", pridevi[random.Next(pridevi.Count)]+" "+ dogadjaji[random.Next(dogadjaji.Count)]);
                queryDict.Add("Description", "U računarstvu, tekst se posmatra apstraktnije. To je bilo koji niz znakova koji je u nekim slučajevima nečitljiv za čoveka.");
                queryDict.Add("Date", RandomDay());
                queryDict.Add("Time", RandomTime().TimeOfDay);
                queryDict.Add("Place", mesto[random.Next(mesto.Count)]);
                queryDict.Add("Address", "Ulica "+random.Next(200));
                queryDict.Add("City", gradovi[random.Next(gradovi.Count)]);
                queryDict.Add("Type", dogadjaji[random.Next(dogadjaji.Count)]);
                queryDict.Add("Creator", users[random.Next(users.Count)].Username);


                client.Cypher
                .Merge("(tip:Tip{ Naziv:{Type} })").With("tip")
                .Merge("(lok:Lokacija{ Naziv:{City}})").With(" tip, lok")
                .Create("(n:Event { id:{id}, Title:{Title}, Description:{Description}, Date:{Date}, Time:{Time},Place:{Place},Address:{Address}})")
                .With("tip, lok, n")
                .Create("(tip)-[sa:PripadaTip]->(n)<-[sb:PripadaGrad]-(lok)")
                .With("tip, lok, n")
                .Match("(user: User{ Username:{Creator} })")
                .With("tip, lok, n, user")
                .Create("(user) -[p: Creator]->(n)")
                .WithParams(queryDict).ExecuteWithoutResults();

                int tmp = random.Next(2, 6);
                for(int j=0;j<tmp;j++)
                {
                    userProvider.GoToEvent(users[random.Next(users.Count)].Username, id);
                }

            }

        }


        public bool DeleteEvent(int id)
        {
            queryDict.Clear();

            queryDict.Add("id", id);

            

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Event { id:{id} }) DETACH DELETE n RETURN Count(n)",
                                                            queryDict, CypherResultMode.Set);

            List<int> numberOfDeletedUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).ToList();

            if (numberOfDeletedUsers[0] == 1)
            {
                return true;
            }

            queryDict.Clear();
            return false;
        }

        public RecEvent GetEvent(int id)
        {
            queryDict.Clear();
            queryDict.Add("id", id);


            RecEvent ev = client.Cypher.OptionalMatch("(n:Event {id:{id}})").With("n")
                .OptionalMatch("(n)<-[r:Creator]-(kreator:User)")
                .With("n,kreator")
                .OptionalMatch("(n)<-[r:GoTo]-(idu:User)")
                .With("n, kreator, idu")
                .OptionalMatch("(tip:Tip)-->(n)<--(lokacija:Lokacija)")
                .With("n, kreator, collect(distinct idu)[..1000] as KoIde, tip, lokacija")
                .WithParams(queryDict)
                .Return((n, kreator, tip, lokacija, KoIde) => new RecEvent
                {
                    ev = n.As<EventNod>(),
                    City = lokacija.As<CityNod>(),
                    Creator = kreator.As<UserNod>(),
                    Type = tip.As<TypeNod>(),
                    Guests = KoIde.As<IEnumerable<UserNod>>()
                }).Results.FirstOrDefault();

            queryDict.Clear();
            return ev;
        }


        public IEnumerable<EventNod> GetEvents(String Grad, String Tip)
        {
            queryDict.Clear();

            queryDict.Add("City", Grad);
            queryDict.Add("Type", Tip);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a {Naziv:{City}})-->(b:Event)<--(c {Naziv:{Type}}) Return b", queryDict, CypherResultMode.Set);

            IEnumerable<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();
            queryDict.Clear();
            return events;

        }


        public IEnumerable<EventNod> GetRecommendedFromFriends(String Username)
        {
            queryDict.Clear();

            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username: {Username}})-[:friend]->(a:User)-[:GoTo]->(e:Event) where NOT(tom) -[:GoTo]->(e)"+
                                                "with e , count(e) as frequency return e ORDER BY frequency DESC LIMIT 10", queryDict, CypherResultMode.Set);

            List<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();
            queryDict.Clear();
            return events;
        }

        public IEnumerable<EventNod> GetRecommendedFromEvents(String Username)
        {
            queryDict.Clear();

            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH ((user:User {Username: {Username}})-[k:GoTo]->(e:Event)<-[:PripadaTip]-(tip: Tip)) with tip, count(tip.Naziv) as TopTip "+
                                        "return tip.Naziv order by TopTip desc limit 1", queryDict, CypherResultMode.Set);

            string topTip = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).SingleOrDefault();

            

            query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username: {Username}})-[:GoTo]->(e:Event), (lok: Lokacija)-->(e) with lok.Naziv as naziv, count(lok.Naziv) as lokacije" +
                                       " return naziv order by lokacije desc limit 1", queryDict, CypherResultMode.Set);

            string topLok = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).FirstOrDefault();

            if(topLok==null)
            {
                topLok = "Nis";
            }
            if (topTip == null)
            {
                topLok ="Zurka";
            }

            queryDict.Add("Lokacija", topLok);
            queryDict.Add("Tip", topTip);


            query = new Neo4jClient.Cypher.CypherQuery("MATCH (e:Lokacija {Naziv:{Lokacija}})-->(p)<--(s:Tip {Naziv:{Tip}}), (n:User{Username:{Username}}) where not (n)-->(p) return distinct p", queryDict, CypherResultMode.Set);

            IEnumerable<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();
            queryDict.Clear();
            return events;

        }

    }
}