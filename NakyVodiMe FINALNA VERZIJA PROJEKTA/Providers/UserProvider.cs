﻿using Neo4j.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Providers
{
    public class UserProvider
    {
        private GraphClient client;
        private Dictionary<string, object> queryDict;
        private static UserProvider up;


        public static UserProvider GetUserProvider()
        {
            if(up==null)
            {
                up = new UserProvider();
            }

            return up;
        }

        private UserProvider()
        {
            queryDict = new Dictionary<string, object>();

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "danoi123");
            try
            {
                client.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            up = this;
        }



        public RecUser RegisterUser(User NewUser)
        {
            queryDict.Clear();
            queryDict.Add("Username", NewUser.Username);
          

            if (!this.CheckIfExist(NewUser.Username))
            {
                return null;
            }

            queryDict.Add("Password", NewUser.Password);
            queryDict.Add("Name", NewUser.Name);
            queryDict.Add("Lastname", NewUser.Lastname);
            queryDict.Add("Searchname", NewUser.Searchname.ToLower());

            RecUser u = client.Cypher.Create("(n:User { Name:{Name}, Lastname:{Lastname}, Username:{Username}, Password:{Password},Searchname:{Searchname} })")
                .WithParams(queryDict)
                .Return((n) => new RecUser
                {
                    user = n.As<UserNod>()
                })
                .Results.FirstOrDefault();

            return u;
        }

        public void PopulateDataBase()
        {

            Random random = new Random();

            string[] n = { "Darko", "Danica", "Vladana", "Stefan", "Dragan", "Dragana", "Ivana", "Dimitrije", "Boban", "Lazar", "Nikola","Stojan", "Nikola", "Danilo", "Aca", "Nemanja","Marija","Slavica","Milorad" };
            string[] l = { "Sarajkic", "Djordjevic", "Stojiljkovic", "Kostic", "Jovanovic", "Davidovac", "Dobric", "Stimac", "Lazic", "Jokic", "Simanic" };
            List<string> names = new List<string>(n);
            List<string> lastnames = new List<string>(l);
            List<User> users = new List<User>();

            for (int i=0;i<50;i++)
            {
                User u=new User();
                
          
                u.Name = names[random.Next(names.Count())];
                u.Lastname = lastnames[random.Next(lastnames.Count())];
                u.Password = u.Name;
                u.Username = u.Name + u.Lastname + random.Next(99).ToString();
                u.Searchname = u.Username.ToLower();

                users.Add(u);

            }

            for (int i = 0; i < users.Count; i++)
            {
                queryDict.Clear();

                if (!this.CheckIfExist(users[i].Username))
                {
                    continue;
                }

                queryDict.Add("Password", users[i].Password);
                queryDict.Add("Name", users[i].Name);
                queryDict.Add("Lastname", users[i].Lastname);
                queryDict.Add("Userame", users[i].Username);
                queryDict.Add("Searchname", users[i].Searchname);

                client.Cypher.Create("(n:User { Name:{Name}, Lastname:{Lastname}, Username:{Username}, Password:{Password},Searchname:{Searchname} })")
                   .WithParams(queryDict)
                   .ExecuteWithoutResults();
            }

            for(int i=0;i<(users.Count)*4;i++)
            {
                string u1, u2;
                u1 = users[random.Next(users.Count)].Username;
                u2= users[random.Next(users.Count)].Username;
                FollowUser(u1, u2);
            }


        }

        public RecUser LogIn(String Username, String Password)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);
            queryDict.Add("Password", Password);



            RecUser ru = client.Cypher.Match("(user:User{Username:{Username}, Password:{Password}})").With("user").OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo=KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;
        }

        public bool DeleteUser(String Username)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User { Username:{Username} }) DETACH DELETE n RETURN Count(n)",
                                                            queryDict, CypherResultMode.Set);

            List<int> numberOfDeletedUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).ToList();

            if (numberOfDeletedUsers[0] == 1)
            {
                return true;
            }

            return false;
        }

        public RecUser GetUser(String Username)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);

            RecUser ru = client.Cypher.Match("(user:User{Username:{Username}})")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public RecUser UpdateUser(String OldUsername, User us)
        {

            queryDict.Clear();

            
            queryDict.Add("Username", us.Username);
           


            if (OldUsername != us.Username && !this.CheckIfExist(us.Username))
            {
                return null;
            }
            queryDict.Add("OldUsername", OldUsername);
            queryDict.Add("Password", us.Password);
            queryDict.Add("Name", us.Name);
            queryDict.Add("Lastname", us.Lastname);
            queryDict.Add("Searchname", us.Searchname.ToLower());

            RecUser ru = client.Cypher.Match("(user:User {Username:{OldUsername}}) SET user.Username={Username}, user.Name={Name}, user.Lastname={Lastname}, user.Password={Password}, user.Searchname={Searchname}")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }


        public RecUser FollowUser(String Username1, String Username2)
        {
            queryDict.Clear();
            queryDict.Add("Username1", Username1);
            queryDict.Add("Username2", Username2);


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User {Username:{Username1}})-[r:friend]->(b:User {Username: {Username2}}) Return a.Username", queryDict, CypherResultMode.Set);

            var hasConnection = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).SingleOrDefault();

            if (hasConnection != null)
            {
                return GetUser(Username1);
            }

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username1} }),(prat:User { Username:{Username2} }) create (user)-[pri: friend]->(prat)")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public RecUser UnFollowUser(String Username1, String Username2)
        {
            queryDict.Clear();
            queryDict.Add("Username1", Username1);
            queryDict.Add("Username2", Username2);



            RecUser ru = client.Cypher.Match("(user:User {Username:{Username1}})-[r:friend]->(b:User {Username: {Username2}}) delete r")
               .With("user")
               .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
               .With("pratioci, user")
               .OptionalMatch("(user)-[p:friend]->(prati:User)")
               .With("pratioci, user , prati")
               .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
               .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public List<UserNod> FindUsers(String ParcialUsername)
        {
            queryDict.Clear();
            queryDict.Add("ParcialUsername", ParcialUsername.ToLower());

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User) WHERE n.Searchname CONTAINS {ParcialUsername} RETURN n", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public List<UserNod> TopUsers()
        {
            queryDict.Clear();
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a)-[:friend]->(b) with b, Count(a) as pratioci return b ORDER BY pratioci DESC LIMIT 20", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public RecUser GoToEvent(string Username, int id)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);
            queryDict.Add("id", id);


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User {Username:{Username}})-[r:GoTo]->(b:Event {id: {id}}) Return a.Username", queryDict, CypherResultMode.Set);

            var hasConnection = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).SingleOrDefault();

            if (hasConnection != null)
            {
                return GetUser(Username);
            }

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username} }),(ev:Event {id:{id}}) create (user)-[pri: GoTo]->(ev)")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;
        }

        public RecUser CancelGoToEvent(string Username, int id)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);
            queryDict.Add("id", id);

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username} }),(ev:Event {id:{id}}), (user)-[pri: GoTo]->(ev) delete pri")
              .With("user")
              .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
              .With("pratioci, user")
              .OptionalMatch("(user)-[p:friend]->(prati:User)")
              .With("pratioci, user , prati")
              .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
              .With("pratioci, user , prati , kreira")
              .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
              .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
              .WithParams(queryDict)
              .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
              .Results.FirstOrDefault();

            return ru;
        }

        public List<UserNod> GetRecommended(String Username)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username:{Username}})-[y:friend]->(a:User)-[y2:friend]->(coFriend:User) WHERE tom <> coFriend "+
                                                                "AND NOT(tom)-[:friend]->(coFriend) with coFriend , count(coFriend) as frequency return coFriend "+
                                                                "ORDER BY frequency DESC LIMIT 10", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public string getPassword(String username)
        {
            queryDict.Clear();
            queryDict.Add("Username", username);

            var query = new Neo4jClient.Cypher.CypherQuery("Match (n:User{Username:{Username}}) return n.Password", queryDict, CypherResultMode.Set);

            return  ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).FirstOrDefault();

        }


        private bool CheckIfExist(String Username)
        {
            queryDict.Clear();
            queryDict.Add("Username", Username);
            var queryC = new Neo4jClient.Cypher.CypherQuery("Match (n:User{Username:{Username}}) return Count(n)",
                                                           queryDict, CypherResultMode.Set);

            int NumberOfUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryC).SingleOrDefault();

            if (NumberOfUsers > 0)
            {
                return false;
            }

            return true;
        }

    }
}