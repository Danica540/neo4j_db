﻿using NakyVodiMe.WinFormControler;
using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe
{
    public partial class EventForm : Form
    {
        RecEvent rc;
        EvProvider ep;
        UEListControl lc;
        public EventForm( )
        {
            InitializeComponent();
            
        }

        public EventForm(int id, UEListControl puc)
        {
            InitializeComponent();
            
            lc = puc;
            ep = EvProvider.GetEvProvider();
            rc = ep.GetEvent(id);

            lbTitle.Text = rc.ev.Title;
            lbDatum.Text = rc.ev.Date.Day.ToString()+"/"+rc.ev.Date.Month.ToString() + "/"+ rc.ev.Date.Year.ToString();
            lbVreme.Text = rc.ev.Time.Hour+":"+rc.ev.Time.Minute + ":" + rc.ev.Time.Second;
            lbMesto.Text = rc.City.Naziv;
            lbTip.Text = rc.Type.Naziv;
            lbAdresa.Text = rc.ev.Address;
            tbOpis.Text = rc.ev.Description;
            lblGc.Text = rc.Guests.Count().ToString();
           
            if (MainForm.CheckGoing(id))
            {
                btnGo.Text = "Cancel";

            }
            else
            {
                btnGo.Text = "Going";
            }

            if(rc.Creator.Username==MainForm.ru.user.Username)
            {
                btnDelete.Visible = true;
            }

        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            MainForm.DeleteEvent(rc.ev.id);
            lc.RefreshData();
            this.Close();
            
        }

        private void btnGo_Click(object sender, EventArgs e)
        {
            if(btnGo.Text=="Cancel")
            {
                MainForm.NotGoing(rc.ev.id);
                btnGo.Text="Going";
                lc.RefreshData();
            }
            else
            {
                MainForm.Going(rc.ev.id);
                btnGo.Text = "Cancel";
                lc.RefreshData();
            }
            this.Close();
        }
    }
}
