﻿namespace NakyVodiMe
{
    partial class EventForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lbAdresa = new System.Windows.Forms.Label();
            this.ltname = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.lbTitle = new System.Windows.Forms.Label();
            this.lbDatum = new System.Windows.Forms.Label();
            this.lbVreme = new System.Windows.Forms.Label();
            this.lbMesto = new System.Windows.Forms.Label();
            this.lbTip = new System.Windows.Forms.Label();
            this.tbOpis = new System.Windows.Forms.TextBox();
            this.btnGo = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblGc = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbAdresa
            // 
            this.lbAdresa.AutoSize = true;
            this.lbAdresa.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbAdresa.Location = new System.Drawing.Point(185, 158);
            this.lbAdresa.Name = "lbAdresa";
            this.lbAdresa.Size = new System.Drawing.Size(110, 25);
            this.lbAdresa.TabIndex = 26;
            this.lbAdresa.Text = "Following:";
            // 
            // ltname
            // 
            this.ltname.AutoSize = true;
            this.ltname.Location = new System.Drawing.Point(86, 54);
            this.ltname.Name = "ltname";
            this.ltname.Size = new System.Drawing.Size(42, 17);
            this.ltname.TabIndex = 16;
            this.ltname.Text = "Date:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(86, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 17);
            this.label3.TabIndex = 17;
            this.label3.Text = "Time:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(87, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 17);
            this.label4.TabIndex = 18;
            this.label4.Text = "City:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(86, 165);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(56, 17);
            this.label5.TabIndex = 19;
            this.label5.Text = "Adress:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(86, 205);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 17);
            this.label6.TabIndex = 20;
            this.label6.Text = "Type:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(187, 273);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(41, 17);
            this.label7.TabIndex = 21;
            this.label7.Text = "Opis:";
            // 
            // lbTitle
            // 
            this.lbTitle.AutoSize = true;
            this.lbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTitle.Location = new System.Drawing.Point(140, 9);
            this.lbTitle.Name = "lbTitle";
            this.lbTitle.Size = new System.Drawing.Size(75, 25);
            this.lbTitle.TabIndex = 22;
            this.lbTitle.Text = "Name:";
            // 
            // lbDatum
            // 
            this.lbDatum.AutoSize = true;
            this.lbDatum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbDatum.Location = new System.Drawing.Point(185, 47);
            this.lbDatum.Name = "lbDatum";
            this.lbDatum.Size = new System.Drawing.Size(113, 25);
            this.lbDatum.TabIndex = 23;
            this.lbDatum.Text = "Lastname:";
            // 
            // lbVreme
            // 
            this.lbVreme.AutoSize = true;
            this.lbVreme.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbVreme.Location = new System.Drawing.Point(185, 83);
            this.lbVreme.Name = "lbVreme";
            this.lbVreme.Size = new System.Drawing.Size(117, 25);
            this.lbVreme.TabIndex = 24;
            this.lbVreme.Text = "Username:";
            // 
            // lbMesto
            // 
            this.lbMesto.AutoSize = true;
            this.lbMesto.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbMesto.Location = new System.Drawing.Point(185, 119);
            this.lbMesto.Name = "lbMesto";
            this.lbMesto.Size = new System.Drawing.Size(111, 25);
            this.lbMesto.TabIndex = 25;
            this.lbMesto.Text = "Followers:";
            // 
            // lbTip
            // 
            this.lbTip.AutoSize = true;
            this.lbTip.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTip.Location = new System.Drawing.Point(185, 198);
            this.lbTip.Name = "lbTip";
            this.lbTip.Size = new System.Drawing.Size(96, 25);
            this.lbTip.TabIndex = 27;
            this.lbTip.Text = "Created:";
            // 
            // tbOpis
            // 
            this.tbOpis.Location = new System.Drawing.Point(75, 306);
            this.tbOpis.Multiline = true;
            this.tbOpis.Name = "tbOpis";
            this.tbOpis.Size = new System.Drawing.Size(274, 97);
            this.tbOpis.TabIndex = 28;
            // 
            // btnGo
            // 
            this.btnGo.Location = new System.Drawing.Point(216, 436);
            this.btnGo.Name = "btnGo";
            this.btnGo.Size = new System.Drawing.Size(133, 68);
            this.btnGo.TabIndex = 29;
            this.btnGo.Text = "button1";
            this.btnGo.UseVisualStyleBackColor = true;
            this.btnGo.Click += new System.EventHandler(this.btnGo_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(75, 436);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(135, 68);
            this.btnDelete.TabIndex = 30;
            this.btnDelete.Text = "DELETE";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Visible = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblGc
            // 
            this.lblGc.AutoSize = true;
            this.lblGc.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGc.Location = new System.Drawing.Point(185, 233);
            this.lblGc.Name = "lblGc";
            this.lblGc.Size = new System.Drawing.Size(96, 25);
            this.lblGc.TabIndex = 32;
            this.lblGc.Text = "Created:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(86, 240);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 17);
            this.label2.TabIndex = 31;
            this.label2.Text = "Guest Count:";
            // 
            // EventForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(432, 547);
            this.Controls.Add(this.lblGc);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnGo);
            this.Controls.Add(this.tbOpis);
            this.Controls.Add(this.lbTip);
            this.Controls.Add(this.lbAdresa);
            this.Controls.Add(this.lbMesto);
            this.Controls.Add(this.lbVreme);
            this.Controls.Add(this.lbDatum);
            this.Controls.Add(this.lbTitle);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.ltname);
            this.Name = "EventForm";
            this.Text = "EventForm";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lbAdresa;
        private System.Windows.Forms.Label ltname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lbTitle;
        private System.Windows.Forms.Label lbDatum;
        private System.Windows.Forms.Label lbVreme;
        private System.Windows.Forms.Label lbMesto;
        private System.Windows.Forms.Label lbTip;
        private System.Windows.Forms.TextBox tbOpis;
        private System.Windows.Forms.Button btnGo;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblGc;
        private System.Windows.Forms.Label label2;
    }
}