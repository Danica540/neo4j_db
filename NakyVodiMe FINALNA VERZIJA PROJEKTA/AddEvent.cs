﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe
{

    public enum Grad
    {
        Nis = 0,
        Beograd = 1,
        NoviSad = 2,
        Kragujevac = 3,
        Leskovac = 4,
        Prokuplje = 5,
        Pirot = 6
    }

    public enum Tip
    {
        Zurka = 0,
        Karaoke = 1,
        Degustacija = 2,
        KnjizevnoVece = 3,
        Izlozba = 4,
        Sajam = 5,
        Predstava = 6
    }

    public partial class AddEvent : Form
    {
        private ProfilUserControl p;
        public AddEvent()
        {
            InitializeComponent();
        }

        public AddEvent(ProfilUserControl puc)
        {
            InitializeComponent();
            p = puc;
            cbTip.DataSource = Enum.GetValues(typeof(Tip));
            cbCity.DataSource = Enum.GetValues(typeof(Grad));
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnCREATE_Click(object sender, EventArgs e)
        {
            EvProvider evProvider = EvProvider.GetEvProvider();
            EventAll ev=new EventAll();
            ev.Title = tbTitle.Text;
            ev.Tip = cbTip.Text;
            ev.Address = tbAdress.Text;
            ev.Creator = MainForm.ru.user.Username;
            ev.Date = dtpDate.Value;
            ev.Description = tbDescription.Text;
            ev.Grad = cbCity.Text;
            ev.Place = tbPlace.Text;
            ev.Time = dtpTime.Value;

            evProvider.AddEvent(ev);

            MainForm.RefreshUser(MainForm.ru.user.Username);
            p.refreshEvents("Created");
            this.Close();
            
        }
    }
}
