﻿namespace NakyVodiMe
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnProfil = new System.Windows.Forms.Button();
            this.btnUsers = new System.Windows.Forms.Button();
            this.btnEvents = new System.Windows.Forms.Button();
            this.ecEvents = new NakyVodiMe.WinFormControl.EventsControl();
            this.profilUserControl1 = new NakyVodiMe.ProfilUserControl();
            this.usersControl1 = new NakyVodiMe.WinFormControl.UsersControl();
            this.SuspendLayout();
            // 
            // btnProfil
            // 
            this.btnProfil.Location = new System.Drawing.Point(12, 18);
            this.btnProfil.Name = "btnProfil";
            this.btnProfil.Size = new System.Drawing.Size(154, 173);
            this.btnProfil.TabIndex = 0;
            this.btnProfil.Text = "PROFIL";
            this.btnProfil.UseVisualStyleBackColor = true;
            this.btnProfil.Click += new System.EventHandler(this.btnProfil_Click);
            // 
            // btnUsers
            // 
            this.btnUsers.Location = new System.Drawing.Point(12, 197);
            this.btnUsers.Name = "btnUsers";
            this.btnUsers.Size = new System.Drawing.Size(154, 178);
            this.btnUsers.TabIndex = 1;
            this.btnUsers.Text = "USERS";
            this.btnUsers.UseVisualStyleBackColor = true;
            this.btnUsers.Click += new System.EventHandler(this.btnUsers_Click);
            // 
            // btnEvents
            // 
            this.btnEvents.Location = new System.Drawing.Point(12, 381);
            this.btnEvents.Name = "btnEvents";
            this.btnEvents.Size = new System.Drawing.Size(154, 178);
            this.btnEvents.TabIndex = 2;
            this.btnEvents.Text = "EVENTS";
            this.btnEvents.UseVisualStyleBackColor = true;
            this.btnEvents.Click += new System.EventHandler(this.btnEvents_Click);
            // 
            // ecEvents
            // 
            this.ecEvents.Location = new System.Drawing.Point(172, 18);
            this.ecEvents.Name = "ecEvents";
            this.ecEvents.Size = new System.Drawing.Size(808, 541);
            this.ecEvents.TabIndex = 4;
            // 
            // profilUserControl1
            // 
            this.profilUserControl1.Location = new System.Drawing.Point(172, 18);
            this.profilUserControl1.Name = "profilUserControl1";
            this.profilUserControl1.Size = new System.Drawing.Size(808, 541);
            this.profilUserControl1.TabIndex = 3;
            // 
            // usersControl1
            // 
            this.usersControl1.Location = new System.Drawing.Point(172, 18);
            this.usersControl1.Name = "usersControl1";
            this.usersControl1.Size = new System.Drawing.Size(808, 541);
            this.usersControl1.TabIndex = 5;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(992, 571);
            this.Controls.Add(this.usersControl1);
            this.Controls.Add(this.ecEvents);
            this.Controls.Add(this.profilUserControl1);
            this.Controls.Add(this.btnEvents);
            this.Controls.Add(this.btnUsers);
            this.Controls.Add(this.btnProfil);
            this.Name = "MainForm";
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnProfil;
        private System.Windows.Forms.Button btnUsers;
        private System.Windows.Forms.Button btnEvents;
        private ProfilUserControl profilUserControl1;
        private WinFormControl.EventsControl ecEvents;
        private WinFormControl.UsersControl usersControl1;
    }
}