﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class UserAll:UserNod
    {
        public IList<UserNod> Following { get; set; } = new List<UserNod>();
        public IList<EventNod> Created { get; set; } = new List<EventNod>();
        public IList<EventNod> GoingTo { get; set; } = new List<EventNod>();
    }
}