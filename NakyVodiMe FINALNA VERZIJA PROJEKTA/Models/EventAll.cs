﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class EventAll : EventNod
    {
        public string Creator { get; set; }
        public string Tip { get; set; }
        public string Grad { get; set; }
        public List<UserNod> Participans{get; set;}

    }
}