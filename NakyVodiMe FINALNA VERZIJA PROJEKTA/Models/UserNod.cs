﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class UserNod
    {
        public String Name { get; set; }
        public String Lastname { get; set; }
        public String Username { get; set; }
        public String Searchname { get; set; }
    }
}