﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class RecEvent
    {
        public EventNod ev { get; set; }
        public CityNod City { get; set; }
        public TypeNod Type { get; set; }
        public UserNod Creator { get; set; }
        public IEnumerable<UserNod> Guests { get; set; }
    }
}