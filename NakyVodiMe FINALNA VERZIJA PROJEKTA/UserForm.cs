﻿using NakyVodiMe.WinFormControler;
using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe
{
    public partial class UserForm : Form
    {
        UserProvider up;
        string user;
        UEListControl userList;
        public UserForm()
        {
            InitializeComponent();
            
        }
       
        public UserForm(String username, UEListControl ulc)
        {
            InitializeComponent();
            user = username;
            userList = ulc;
            up = UserProvider.GetUserProvider();

            RecUser ru = up.GetUser(username);
            lbName.Text = ru.user.Name;
            lbLastname.Text = ru.user.Lastname;
            lbUsername.Text = ru.user.Username;
            lbFollowers.Text = ru.Followers.Count().ToString();
            lbFollowing.Text = ru.Following.Count().ToString();
            lbCreated.Text = ru.Created.Count().ToString();
            lbGoing.Text = ru.GoingTo.Count().ToString();

            if(MainForm.CheckFollowing(username))
            {
                button1.Text = "UnFollow";
                
            }
            else
            {
                button1.Text = "Follow";
            }
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(button1.Text=="Follow")
            {
                MainForm.Follow(user);
                button1.Text= "UnFollow";
                lbFollowers.Text = (int.Parse(lbFollowers.Text) + 1).ToString();
                userList.RefreshData();
            }
            else
            {
                MainForm.UnFollow(user);
                button1.Text = "Follow";
                lbFollowers.Text = (int.Parse(lbFollowers.Text) - 1).ToString();
                userList.RefreshData();
            }

            this.Close();

        }


    }
}
