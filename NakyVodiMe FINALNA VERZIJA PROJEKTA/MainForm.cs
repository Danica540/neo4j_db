﻿using Neo4j.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NakyVodiMe.WinFormControler;
using Neo4j.Providers;

namespace NakyVodiMe
{
    public partial class MainForm : Form
    {
        public static RecUser ru;
        private static UserProvider uProvider;
        private static EvProvider eProvider;
        public MainForm()
        {
            InitializeComponent();
        }

        public MainForm(RecUser user)
        {
            InitializeComponent();
            ru = user;
            uProvider = UserProvider.GetUserProvider();
            eProvider = EvProvider.GetEvProvider();

            profilUserControl1.BringToFront();
            profilUserControl1.getUser(ru);
            profilUserControl1.setForm(this);
            btnProfil.PerformClick();
        }

        private void btnProfil_Click(object sender, EventArgs e)
        {
            hideControls();
            profilUserControl1.Visible = true;
            profilUserControl1.BringToFront();

        }

        public static bool CheckFollowing(String username)
        {
            foreach(UserNod u in ru.Following)
            {
                if (u.Username == username)
                    return true;
            }
            return false;
        }

        public static bool CheckGoing(int id)
        {
            foreach (EventNod u in ru.GoingTo)
            {
                if (u.id == id)
                    return true;
            }
            return false;
        }

        public static void updateUser(User un)
        {
            if(un.Password=="")
            {
                un.Password = uProvider.getPassword(ru.user.Username);
            }
            ru = uProvider.UpdateUser(ru.user.Username, un);
        }

        public static void DeleteEvent(int id)
        {
            eProvider.DeleteEvent(id);
            RefreshUser(ru.user.Username);
        }

        public static void Going(int id)
        {
            ru = uProvider.GoToEvent(ru.user.Username, id);
        }

        public static void NotGoing(int id)
        {
            ru = uProvider.CancelGoToEvent(ru.user.Username, id);
        }

        public static void UnFollow(String username)
        {
            ru = uProvider.UnFollowUser(ru.user.Username, username);
        }


        public static void Follow(String username)
        {
            ru = uProvider.FollowUser(ru.user.Username, username);
        }

        private void btnUsers_Click(object sender, EventArgs e)
        {
            hideControls();
            usersControl1.Visible = true;
            usersControl1.ShowTopUsers();
            usersControl1.BringToFront();

        }




        public static void RefreshUser(String username)
        {
            ru = uProvider.GetUser(username);
        }

        private void hideControls()
        {
            ecEvents.Visible = false;
            profilUserControl1.Visible = false;
        }

        private void btnEvents_Click(object sender, EventArgs e)
        {
            hideControls();
            ecEvents.Visible = true;
            ecEvents.BringToFront();
            ecEvents.showTrending();
        }
    }
}
