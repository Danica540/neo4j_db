﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NakyVodiMe
{
    public partial class EditUser : Form
    {
        ProfilUserControl par;
        string uname;
        public EditUser()
        {
            InitializeComponent();
        }

        public EditUser(string  username, ProfilUserControl parent)
        {
            InitializeComponent();
            par = parent;
            UserNod un = UserProvider.GetUserProvider().GetUser(username).user;
            tbName.Text = un.Name;
            uname = un.Username;
            tbLastname.Text = un.Lastname;
            tbUsername.Text = un.Username;
            tbPassword.PasswordChar = '*';
            tbRePassword.PasswordChar = '*';

        }



        private void button1_Click(object sender, EventArgs e)
        {
 
            if(tbPassword.Text!=tbRePassword.Text)
            {
                MessageBox.Show("password must match");
            }
            else if(tbUsername.Text != uname && UserProvider.GetUserProvider().GetUser(tbUsername.Text)!=null)
            {
                MessageBox.Show("Username is taken");
            }
            else
            {
                User u = new User();
                u.Name = tbName.Text;
                u.Lastname = tbLastname.Text;
                u.Password = tbPassword.Text;
                u.Username = tbUsername.Text;
                u.Searchname = tbUsername.Text.ToLower();
                par.updatelabel(u.Name, u.Lastname, u.Username);

                MainForm.updateUser(u);
                this.Close();
            }

            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
