﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LogInController : ApiController
    {
        
        // POST: api/LogIn
        public HttpResponseMessage Post([FromBody]LogInRequest value)
        {
            UserProvider up = new UserProvider();
            RecUser user = up.LogIn(value);

            if (user.user == null)
                return Request.CreateResponse(HttpStatusCode.OK, "LogIn failed");

            return Request.CreateResponse(HttpStatusCode.OK, user);

        }
    }
}
