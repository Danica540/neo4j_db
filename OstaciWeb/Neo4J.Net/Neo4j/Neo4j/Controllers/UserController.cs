﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UserController : ApiController
    {
        // GET: api/User
        public HttpResponseMessage Get()
        {
            UserProvider up = new UserProvider();
            List<UserNod> users = up.TopUsers();
            if (users == null)
                return Request.CreateResponse(HttpStatusCode.OK, "Error in top users");
            return Request.CreateResponse(HttpStatusCode.OK, users);
        }

        // GET: api/User/?Username=a
        public HttpResponseMessage Get(String Username)
        {
            UserProvider up = new UserProvider();
            RecUser user = up.GetUser(Username);

            if(user.user==null)
                return Request.CreateResponse(HttpStatusCode.OK, "User doesen't exist");
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        // POST: api/User
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/User/?Username=a
        public HttpResponseMessage Put(String Username, [FromBody]User user)
        {
            UserProvider up = new UserProvider();
            RecUser u = up.UpdateUser(Username, user);
            if (u.user == null)
                return Request.CreateResponse(HttpStatusCode.BadRequest, "Error updating user");

            return Request.CreateResponse(HttpStatusCode.OK, u);
        }

        // DELETE: api/User/?Username=a
        public HttpResponseMessage Delete(String Username)
        {
            UserProvider up = new UserProvider();
            if (up.DeleteUser(Username))
                return Request.CreateResponse(HttpStatusCode.OK, "Successfully deleted");

            return Request.CreateResponse(HttpStatusCode.BadRequest, "Error while deleting user");
        }
    }
}
