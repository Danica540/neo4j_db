﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Neo4j.Controllers
{
    public class GoToController : ApiController
    {
        // GET: api/GoTo
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/GoTo/?Username=a&Id=b
        public HttpResponseMessage Get(String Username, int id)
        {
            UserProvider up = new UserProvider();
            RecUser ru = up.GoToEvent(Username, id);
            if (ru.user == null)
                return Request.CreateResponse(HttpStatusCode.OK, "error going to");
            return Request.CreateResponse(HttpStatusCode.OK, ru);
        }

        // POST: api/GoTo
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/GoTo/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/GoTo/?Username=a&Id=b
        public HttpResponseMessage Delete(String Username, int id)
        {
            UserProvider up = new UserProvider();
            RecUser ru = up.CancelGoToEvent(Username, id);
            if (ru.user == null)
                return Request.CreateResponse(HttpStatusCode.OK, "error going to");
            return Request.CreateResponse(HttpStatusCode.OK, ru);
        }
    }
}
