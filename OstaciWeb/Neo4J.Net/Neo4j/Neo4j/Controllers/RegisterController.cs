﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegisterController : ApiController
    {
        

        // POST: api/Register
        public HttpResponseMessage Post([FromBody]User value)
        {

            UserProvider up = new UserProvider();
            RecUser user=up.RegisterUser(value);

            if(user.user==null)
               return  Request.CreateResponse(HttpStatusCode.OK, "Duplicated username");

            return Request.CreateResponse(HttpStatusCode.OK, user);
        }
    }
}
