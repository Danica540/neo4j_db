﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FollowController : ApiController
    {
        // GET: api/Follow
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Follow/?Username1=a&Username2=b
        public HttpResponseMessage Get(String Username1, String Username2)
        {
            UserProvider up = new UserProvider();
            RecUser user = up.FollowUser(Username1, Username2);
            if (user.user == null)
                return Request.CreateResponse(HttpStatusCode.OK, "Error in follow");
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }

        // POST: api/Follow
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Follow/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Follow/?Username1=a&Username2=b
        public HttpResponseMessage Delete(String Username1, String Username2)
        {
            UserProvider up = new UserProvider();
            RecUser user = up.UnFollowUser(Username1, Username2);
            if (user.user == null)
                return Request.CreateResponse(HttpStatusCode.OK, "Error in unfollow");
            return Request.CreateResponse(HttpStatusCode.OK, user);
        }
    }
}
