﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SearchUserController : ApiController
    {
        // GET: api/SearchUser/?ParcialUsername=a
        public HttpResponseMessage Get(string ParcialUsername)
        {
            UserProvider up = new UserProvider();
            List<UserNod> users = up.FindUsers(ParcialUsername);
            if (users == null)
                return Request.CreateResponse(HttpStatusCode.OK, "users not found");
            return Request.CreateResponse(HttpStatusCode.OK, users);
        }

       
    }
}
