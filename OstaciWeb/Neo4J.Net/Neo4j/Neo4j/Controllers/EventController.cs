﻿using Neo4j.Models;
using Neo4j.Providers;
using System;
using System.Collections.Generic;
using System.Diagnostics.Eventing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Neo4j.Controllers
{

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EventController : ApiController
    {
        // GET: api/Event
        public void Get()
        {
            
        }

        // GET: api/Event/5
        public HttpResponseMessage Get(int id)
        {
            EvProvider ep = new EvProvider();
            RecEvent ev = ep.GetEvent(id);

            if (ev.ev == null)
                return Request.CreateResponse(HttpStatusCode.OK, "error in getting event");

            return Request.CreateResponse(HttpStatusCode.OK, ev);
        }

        // GET: api/Event/?Lokacija=a&Tip=b
        public HttpResponseMessage Get(String Lokacija, String Tip)
        {
            EvProvider ep = new EvProvider();
            List<EventNod> ev = ep.GetEvents(Lokacija,Tip);

            if (ev == null)
                return Request.CreateResponse(HttpStatusCode.OK, "error in getting event");

            return Request.CreateResponse(HttpStatusCode.OK, ev);
        }

        // POST: api/Event
        public HttpResponseMessage Post([FromBody]EventAll value)
        {
            EvProvider ep = new EvProvider();
            RecEvent ev = ep.AddEvent(value);

            if (ev.ev == null)
                return Request.CreateResponse(HttpStatusCode.OK, "error in creating event");

            return Request.CreateResponse(HttpStatusCode.OK, ev);

        }

        // PUT: api/Event/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Event/5
        public HttpResponseMessage Delete(int id)
        {
            EvProvider ep = new EvProvider();
            if(ep.DeleteEvent(id))
                return Request.CreateResponse(HttpStatusCode.OK, "event delited");
            return Request.CreateResponse(HttpStatusCode.OK, "event successfully deleted");

        }
    }
}
