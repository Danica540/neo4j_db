﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class LogInRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }
}