﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class RecUser
    {
        public UserNod user { get; set; }
        public IEnumerable<UserNod> Following { get; set; } = new List<UserNod>();
        public IEnumerable<UserNod> Followers { get; set; } = new List<UserNod>();
        public IEnumerable<EventNod> Created { get; set; } = new List<EventNod>();
        public IEnumerable<EventNod> GoingTo { get; set; } = new List<EventNod>();
    }
}