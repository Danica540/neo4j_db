﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Models
{
    public class EventNod
    {
        public int id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public DateTime Time { get; set; }
        public string Place { get; set; }
        public string Address { get; set; }
    }
}