﻿using Neo4j.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Providers
{
    public class UserProvider
    {
        private GraphClient client;
        private Dictionary<string, object> queryDict;

        public UserProvider()
        {
            queryDict = new Dictionary<string, object>();

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "danoi123");
            try
            {
                client.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }



        public RecUser RegisterUser(User NewUser)
        {
            queryDict.Add("Username", NewUser.Username);
            queryDict.Add("Password", NewUser.Password);
            queryDict.Add("Name", NewUser.Name);
            queryDict.Add("Lastname", NewUser.Lastname);
            queryDict.Add("Searchname", NewUser.Searchname.ToLower());




            if (!this.CheckIfExist(NewUser.Username))
            {
                return null;
            }

            RecUser u = client.Cypher.Create("(n:User { Name:{Name}, Lastname:{Lastname}, Username:{Username}, Password:{Password},Searchname:{Searchname} })")
                .WithParams(queryDict)
                .Return((n) => new RecUser
                {
                    user = n.As<UserNod>()
                })
                .Results.FirstOrDefault();

            return u;
        }

        public RecUser LogIn(LogInRequest lr)
        {

            queryDict.Add("Username", lr.Username);
            queryDict.Add("Password", lr.Password);



            RecUser ru = client.Cypher.OptionalMatch("(pratioci:User)-[r:friend]->(user:User{Username:{Username}, Password:{Password}})")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo=KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();




            return ru;
        }

        public bool DeleteUser(String Username)
        {
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User { Username:{Username} }) DETACH DELETE n RETURN Count(n)",
                                                            queryDict, CypherResultMode.Set);

            List<int> numberOfDeletedUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).ToList();

            if (numberOfDeletedUsers[0] == 1)
            {
                return true;
            }

            return false;
        }

        public RecUser GetUser(String Username)
        {
            queryDict.Add("Username", Username);

            RecUser ru = client.Cypher.OptionalMatch("(pratioci:User)-[r:friend]->(user:User{Username:{Username}})")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public RecUser UpdateUser(String OldUsername, User us)
        {


            queryDict.Add("OldUsername", OldUsername);
            queryDict.Add("Username", us.Username);
            queryDict.Add("Password", us.Password);
            queryDict.Add("Name", us.Name);
            queryDict.Add("Lastname", us.Lastname);
            queryDict.Add("Searchname", us.Searchname.ToLower());




            if (OldUsername != us.Username && !this.CheckIfExist(us.Username))
            {
                return null;
            }


            RecUser ru = client.Cypher.Match("(user:User {Username:{OldUsername}}) SET user.Username={Username}, user.Name={Name}, user.Lastname={Lastname}, user.Password={Password}, user.Searchname={Searchname}")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user:User{Username:{Username}})")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }


        public RecUser FollowUser(String Username1, String Username2)
        {
            queryDict.Add("Username1", Username1);
            queryDict.Add("Username2", Username2);


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User {Username:{Username1}})-[r:friend]->(b:User {Username: {Username2}}) Return a.Username", queryDict, CypherResultMode.Set);

            var hasConnection = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).SingleOrDefault();

            if (hasConnection != null)
            {
                return GetUser(Username1);
            }

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username1} }),(prat:User { Username:{Username2} }) create (user)-[pri: friend]->(prat)")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public RecUser UnFollowUser(String Username1, String Username2)
        {
            queryDict.Add("Username1", Username1);
            queryDict.Add("Username2", Username2);



            RecUser ru = client.Cypher.Match("(user:User {Username:{Username1}})-[r:friend]->(b:User {Username: {Username2}}) delete r")
               .With("user")
               .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
               .With("pratioci, user")
               .OptionalMatch("(user)-[p:friend]->(prati:User)")
               .With("pratioci, user , prati")
               .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
               .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;

        }

        public List<UserNod> FindUsers(String ParcialUsername)
        {
            queryDict.Add("ParcialUsername", ParcialUsername.ToLower());

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:User) WHERE n.Searchname CONTAINS {ParcialUsername} RETURN n", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public List<UserNod> TopUsers()
        {
            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a)-[:friend]->(b) with b, a as pratioci return b,Count(pratioci) as broj ORDER BY broj DESC LIMIT 20", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }

        public RecUser GoToEvent(string Username, int id)
        {
            queryDict.Add("Username", Username);
            queryDict.Add("id", id);


            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a:User {Username:{Username}})-[r:GoTo]->(b:Event {id: {id}}) Return a.Username", queryDict, CypherResultMode.Set);

            var hasConnection = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).SingleOrDefault();

            if (hasConnection != null)
            {
                return GetUser(Username);
            }

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username} }),(ev:Event {id:{id}}) create (user)-[pri: GoTo]->(ev)")
                .With("user")
                .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
                .With("pratioci, user")
                .OptionalMatch("(user)-[p:friend]->(prati:User)")
                .With("pratioci, user , prati")
                .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
                .With("pratioci, user , prati , kreira")
                .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
                .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
                .WithParams(queryDict)
                .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
               .Results.FirstOrDefault();

            return ru;
        }

        public RecUser CancelGoToEvent(string Username, int id)
        {
            queryDict.Add("Username", Username);
            queryDict.Add("id", id);

            RecUser ru = client.Cypher.Match("(user:User { Username:{Username} }),(ev:Event {id:{id}}), (user)-[pri: GoTo]->(ev) delete pri")
              .With("user")
              .OptionalMatch("(pratioci:User)-[r:friend]->(user)")
              .With("pratioci, user")
              .OptionalMatch("(user)-[p:friend]->(prati:User)")
              .With("pratioci, user , prati")
              .OptionalMatch("(kreira:Event)<-[s:Creator]-(user)")
              .With("pratioci, user , prati , kreira")
              .OptionalMatch("(ideNa:Event)<-[l:GoTo]-(user)")
              .With("collect(distinct pratioci)[..1000] as KorPratioci, user, collect(distinct prati)[..1000] as KorPrati, collect(distinct kreira)[..1000] as KorKreira,  collect(distinct ideNa)[..1000] as KorIdeNa")
              .WithParams(queryDict)
              .Return((KorPratioci, user, KorPrati, KorKreira, KorIdeNa) => new RecUser
                {
                    user = user.As<UserNod>(),
                    Following = KorPrati.As<IEnumerable<UserNod>>(),
                    Followers = KorPratioci.As<IEnumerable<UserNod>>(),
                    Created = KorKreira.As<IEnumerable<EventNod>>(),
                    GoingTo = KorIdeNa.As<IEnumerable<EventNod>>()
                })
              .Results.FirstOrDefault();

            return ru;
        }

        public List<UserNod> GetRecommended(String Username)
        {
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username:{Username}})-[:friend]->(a:User)-[:friend]->(coFriend:User) WHERE tom <> coFriend"+
                                                                "AND NOT(tom) -[:friend]->(coFriend) with coFriend , count(coFriend) as frequency return coFriend, frequency"+
                                                                "ORDER BY frequency DESC LIMIT 10", queryDict, CypherResultMode.Set);

            List<UserNod> users = ((IRawGraphClient)client).ExecuteGetCypherResults<UserNod>(query).ToList();

            if (users == null)
            {
                return null;
            }

            return users;
        }


        private bool CheckIfExist(String Username)
        {
            var queryC = new Neo4jClient.Cypher.CypherQuery("start n=node(*) where (n:User) and exists(n.Username) and n.Username =~ {Username} return Count(n)",
                                                           queryDict, CypherResultMode.Set);

            int NumberOfUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(queryC).SingleOrDefault();

            if (NumberOfUsers > 0)
            {
                return false;
            }

            return true;
        }

    }
}