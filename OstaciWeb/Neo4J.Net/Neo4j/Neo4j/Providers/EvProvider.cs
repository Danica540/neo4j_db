﻿using Neo4j.Models;
using Neo4jClient;
using Neo4jClient.Cypher;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Neo4j.Providers
{
    public class EvProvider
    {
        private GraphClient client;
        private Dictionary<string, object> queryDict;
        private EventNod ev;

        public EvProvider()
        {
            queryDict = new Dictionary<string, object>();

            client = new GraphClient(new Uri("http://localhost:7474/db/data"), "neo4j", "danoi123");
            try
            {
                client.Connect();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public RecEvent AddEvent(EventAll e)
        {
         
            int id;

            queryDict.Add("Title", e.Title);
            queryDict.Add("Description", e.Description);
            queryDict.Add("Date", e.Date.Date);
            queryDict.Add("Time", e.Time.TimeOfDay);
            queryDict.Add("Place", e.Place);
            queryDict.Add("Address", e.Address);
            queryDict.Add("City", e.Grad);
            queryDict.Add("Type", e.Tip);
            queryDict.Add("Creator", e.Creator);

            var query = new Neo4jClient.Cypher.CypherQuery("MERGE (id:GlobalUniqueId) ON CREATE SET id.count = 1 ON MATCH SET id.count = id.count + 1 RETURN id.count AS generated_id",
                                                          queryDict, CypherResultMode.Set);

            id = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).SingleOrDefault();

            if (id == null)
            {
                return null;
            }
            queryDict.Add("id", id);


            RecEvent ev = client.Cypher
                .Merge("(tip:Tip{ Naziv:{Type} })").With("tip")
                .Merge("(lok:Lokacija{ Naziv:{City}})").With(" tip, lok")
                .Create("(n:Event { id:{id}, Title:{Title}, Description:{Description}, Date:{Date}, Time:{Time},Place:{Place},Address:{Address}})")
                .With("tip, lok, n")
                .Create("(tip)-[sa:PripadaTip]->(n)<-[sb:PripadaGrad]-(lok)")
                .With("tip, lok, n")
                .Match("(user: User{ Username:{Creator} })")
                .With("tip, lok, n, user")
                .Create("(user) -[p: Creator]->(n)")
                .WithParams(queryDict)
                .Return((n, user, tip, lok) => new RecEvent
                {
                    ev = n.As<EventNod>(),
                    City = lok.As<CityNod>(),
                    Creator = user.As<UserNod>(),
                    Type = tip.As<TypeNod>()
                }).Results.FirstOrDefault();


            return ev;
        }


        public bool DeleteEvent(int id)
        {
            queryDict.Add("id", id);

            

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (n:Event { id:{id} }) DETACH DELETE n RETURN Count(n)",
                                                            queryDict, CypherResultMode.Set);

            List<int> numberOfDeletedUsers = ((IRawGraphClient)client).ExecuteGetCypherResults<int>(query).ToList();

            if (numberOfDeletedUsers[0] == 1)
            {
                return true;
            }

            return false;
        }

        public RecEvent GetEvent(int id)
        {
            queryDict.Add("id", id);


            RecEvent ev = client.Cypher
                .Match("(n:Event { id:{id}})<-[r:Creator]-(kreator:User)")
                .With("n,kreator")
                .Match("(n:Event { id:{id}})<-[r:GoTo]-(idu:User)")
                .With("n, kreator, idu")
                .Match("(tip:Tip)-->(n)<--(lokacija:Lokacija)")
                .With("n, kreator, collect(distinct idu)[..1000] as KoIde, tip, lokacija")
                .WithParams(queryDict)
                .Return((n, kreator, tip, lokacija, KoIde) => new RecEvent
                {
                    ev = n.As<EventNod>(),
                    City = lokacija.As<CityNod>(),
                    Creator = kreator.As<UserNod>(),
                    Type = tip.As<TypeNod>(),
                    Guests = KoIde.As<IEnumerable<UserNod>>()
                }).Results.FirstOrDefault();


            return ev;
        }


        public List<EventNod> GetEvents(String Grad, String Tip)
        {
            queryDict.Add("City", Grad);
            queryDict.Add("Type", Tip);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (a {Naziv:{City}})-->(b:Event)<--(c {Naziv:{Type}}) Return b", queryDict, CypherResultMode.Set);

            List<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();

            return events;

        }


        public List<EventNod> GetRecommendedFromFriends(String Username)
        {
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username: 'testUser2'})-[:friend]->(a:User)-[:GoTo]->(e:Event) where NOT(tom) -[:GoTo]->(e)"+
                                                "with e , count(e) as frequency return e, frequency ORDER BY frequency DESC LIMIT 10", queryDict, CypherResultMode.Set);

            List<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();

            return events;
        }

        public List<EventNod> GetRecommendedFromEvents(String Username)
        {
            queryDict.Add("Username", Username);

            var query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username: {Username}})-[:GoTo]->(e:Event), (tip: Tip)--(e) with tip.Naziv as naziv, count(tip.Naziv) as tipovi"+
                                        "return naziv order by tipovi desc limit 1", queryDict, CypherResultMode.Set);

            string topTip = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).FirstOrDefault();

            query = new Neo4jClient.Cypher.CypherQuery("MATCH (tom:User {Username: {Username}})-[:GoTo]->(e:Event), (lok: Lokacija)--(e) with lok.Naziv as naziv, count(lok.Naziv) as lokacije" +
                                       "return naziv order by lokacije desc limit 1", queryDict, CypherResultMode.Set);

            string topLok = ((IRawGraphClient)client).ExecuteGetCypherResults<string>(query).FirstOrDefault();

            queryDict.Add("Lokacija", topLok);
            queryDict.Add("Tip", topTip);

            query = new Neo4jClient.Cypher.CypherQuery("MATCH (e:Lokacija {Naziv:{Lokacija}})-->(p)<--(s:Tip {Naziv:{Tip}}) return distinct p", queryDict, CypherResultMode.Set);

            List<EventNod> events = ((IRawGraphClient)client).ExecuteGetCypherResults<EventNod>(query).ToList();

            return events;

        }

    }
}