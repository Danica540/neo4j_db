export const environmentVariables = {
    JSON_API_URL: 'http://localhost:3003',
    NEO4J_API_URL: "https://localhost:44322/api"
}