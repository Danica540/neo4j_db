import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from "@angular/common/http";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MenuComponent } from './components/menu/menu.component';
import { HomeComponent } from './components/home/home.component';
import { StarterComponent } from './components/starter/starter.component';
import { EventsComponent } from './components/events/events.component';
import { FriendsComponent } from './components/friends/friends.component';
import { ProfileComponent } from './components/profile/profile.component';
import { SingleEventComponent } from './components/single-event/single-event.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { PeopleListComponent } from './components/people-list/people-list.component';
import { AddEventComponent } from './components/add-event/add-event.component';
import { PersonComponent } from './components/person/person.component';

@NgModule({
  declarations: [
    AppComponent,
    MenuComponent,
    HomeComponent,
    StarterComponent,
    EventsComponent,
    FriendsComponent,
    ProfileComponent,
    SingleEventComponent,
    EventDetailsComponent,
    PeopleListComponent,
    AddEventComponent,
    PersonComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
