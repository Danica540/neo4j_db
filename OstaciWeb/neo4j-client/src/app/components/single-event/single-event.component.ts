import { Component, OnInit, Input } from '@angular/core';
import { Event } from 'src/app/models/Event';
import { returnFormatedDate } from 'src/app/functions/formatingFunctions';
import { Router } from '@angular/router';

@Component({
  selector: 'app-event',
  templateUrl: './single-event.component.html',
  styleUrls: ['./single-event.component.scss']
})
export class SingleEventComponent implements OnInit {

  @Input() event: Event;
  eventImage: string = "../../../assets/noImage.png";

  constructor(private router:Router) { }

  ngOnInit() {
    this.eventImage = "../../../assets/image" + (this.event.id % 19) + ".png";
  }

  returnFormatedAddress() {
    return this.event.address.concat(`, ${this.event.city}`);
  }

  returnFormatedDate() {
    return returnFormatedDate(this.event.date);
  }

  returnFormatedDescription() {
    return this.event.description.substr(0, 100) + "..."
  }

  onEventClick() {
    this.router.navigate(["/events", this.event.id]);
  }

}
