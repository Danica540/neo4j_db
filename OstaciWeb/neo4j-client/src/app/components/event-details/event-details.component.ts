import { Component, OnInit } from '@angular/core';
import { Event } from 'src/app/models/Event';
import { ActivatedRoute, Params } from '@angular/router';
import { UserHasEvent } from 'src/app/models/UserHasEvent';
import { EventsService } from 'src/app/services/events.service';
import { UserService } from 'src/app/services/user.service';
import { returnFormatedDate } from 'src/app/functions/formatingFunctions';

@Component({
  selector: 'app-event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent implements OnInit {

  eventId: number;
  event: Event;
  isLoggedIn: boolean;
  userId: number;
  userResponse: UserHasEvent;
  numberOfPeopleComing: number = 0;
  isNumberOfPeopleMax: boolean = false;
  eventImage: string = "../../../assets/noImage.png";

  constructor(private eventsService: EventsService, private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.eventId = parseInt(params["eventId"]);
      this.getEvent(this.eventId);
    })
    if (localStorage.getItem("isLoggedIn") === "true") {
      this.isLoggedIn = true;
      this.userId = parseInt(localStorage.getItem("userId"));
      this.getUserResponse(this.userId, this.eventId);
    }
  }

  getEvent(id: number) {
    this.eventsService.getEvent(id).subscribe((event: Event) => {
      console.log(event);
      if (event) {
        this.event = event;
        this.eventImage = "../../../assets/image" + (this.event.id % 19) + ".png";
        this.numberOfPeopleComing = this.event.listOfPeopleComing.length;
      }
    })
  }

  getUserResponse(userId: number, eventId: number) {
    this.userService.getResponse(userId, eventId).subscribe((res: UserHasEvent) => {
      console.log(res);
      this.userResponse = res;
      if (res) {
        this.updateButtonStyle("coming");
      }
      else {
        this.updateButtonStyle("not coming");
      }
    })
  }

  registerCommingEvent() {
    this.userResponse = this.returnNewResponse();
    this.userService.addResponse(this.userResponse);
    this.incrementNumberOfPeopleComming();
    this.updateEvent(this.event);
    this.updateButtonStyle("coming");
  }

  unregisterCommingEvent() {
    this.userService.deleteResponse(this.userResponse.id);
    this.userResponse = null;
    this.decrementNumberOfPeopleComming();
    this.updateEvent(this.event);
    this.updateButtonStyle("not coming");
  }

  updateButtonStyle(mode: string) {
    if (mode === "coming") {
      if ((document.getElementById('yesBtn') as any) && (document.getElementById('noBtn') as any)) {
        (document.getElementById('yesBtn') as any).disabled = true;
        (document.getElementById('yesBtn') as any).className = "btn btn-secondary m-2";
        (document.getElementById('noBtn') as any).disabled = false;
        (document.getElementById('noBtn') as any).className = "btn btn-light m-2";
      }

    }
    else {
      if ((document.getElementById('yesBtn') as any) && (document.getElementById('noBtn') as any)) {
        (document.getElementById('yesBtn') as any).disabled = false;
        (document.getElementById('yesBtn') as any).className = "btn btn-light m-2";
        (document.getElementById('noBtn') as any).disabled = true;
        (document.getElementById('noBtn') as any).className = "btn btn-secondary m-2";
      }
    }
  }

  returnNewResponse() {
    let newResponse = new UserHasEvent();
    newResponse.setAttributes(this.eventId, this.userId, true);
    return newResponse;
  }

  incrementNumberOfPeopleComming() {
    this.numberOfPeopleComing += 1;
  }

  updateEvent(event: Event) {
    this.eventsService.updateEvent(event).subscribe(res => {
      console.log(res);
    })
  }

  decrementNumberOfPeopleComming() {
    this.numberOfPeopleComing -= 1;
  }

  returnFormatedDate() {
    return returnFormatedDate(this.event.date);
  }

}
