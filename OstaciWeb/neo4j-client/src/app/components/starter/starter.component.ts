import { Component, OnInit } from '@angular/core';
import { AccessService } from 'src/app/services/access.service';
import { setLocalStorage, clearLocalStorage } from 'src/app/functions/localStorageFunctions';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-starter',
  templateUrl: './starter.component.html',
  styleUrls: ['./starter.component.scss']
})
export class StarterComponent implements OnInit {

  constructor(public accessService: AccessService) {
  }

  ngOnInit() {
    clearLocalStorage();
  }

  onSignIn() {
    let usernameSingInInput = document.getElementById("usernameSignIn") as HTMLInputElement;
    let passwordSignInInput = document.getElementById("passwordSignIn") as HTMLInputElement;

    this.accessService.signInUser(usernameSingInInput.value, passwordSignInInput.value).subscribe((res: any) => {
      if (res === "LogIn failed") {
        document.getElementById("error").innerHTML = "Pogresna sifra ili username."
        // pogresna sifra ili username
      }
      else {
        setLocalStorage(usernameSingInInput.value, null);
        location.replace('home');
      }

    })

  }

  onSignUp() {
    let usernameSingUpInput = document.getElementById("usernameSignUp") as HTMLInputElement;
    let passwordSignUpInput = document.getElementById("passwordSignUp") as HTMLInputElement;
    let nameSingUpInput = document.getElementById("nameSignUp") as HTMLInputElement;
    let lastnameSignUpInput = document.getElementById("lastnameSignUp") as HTMLInputElement;

    /*
    username: string;
    password: string;
    name: string;
    lastname: string;
    searchname: string;*/

    this.accessService.signUpUser(usernameSingUpInput.value,
      passwordSignUpInput.value, nameSingUpInput.value, lastnameSignUpInput.value).subscribe((res: any) => {
        if (res === "Duplicated username") {
          document.getElementById("error").innerHTML = "Korisnicko ime je zauzeto."
          // korisnicko ime zauzeto
        }
        else {
          setLocalStorage(usernameSingUpInput.value, null);
          location.replace('home');
        }
        console.log(res);

      });
  }

}
