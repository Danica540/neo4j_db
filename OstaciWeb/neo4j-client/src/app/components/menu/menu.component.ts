import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { clearLocalStorage } from 'src/app/functions/localStorageFunctions';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }

  gotoProfile(){
    this.router.navigate(['/user',localStorage.getItem('username')])
  }

  onSignOut(){
    clearLocalStorage();
    this.router.navigate(['/'])
  }

}
