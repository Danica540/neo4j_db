import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { UserHasEvent } from 'src/app/models/UserHasEvent';
import { UserService } from 'src/app/services/user.service';
import { UserHasFriend } from 'src/app/models/UserHasFriend';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  user: any;
  loggedUser: any;
  isLoggedIn: boolean;
  userImage: string = "../../../assets/noImage.png";
  coverImage: string = "../../../assets/cover0.jfif";
  isFriends = false;
  followers:number=0;
  following:number=0;

  constructor(private userService: UserService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.username = params["userId"];
      this.getUser(this.username);
    })
    if (localStorage.getItem("isLoggedIn") === "true") {
      this.isLoggedIn = true;
      this.getUserResponses(this.username);
    }
  }

  getUser(username: string) {
    this.userService.getUser(username).subscribe((user: any) => {
      console.log(user[0]);
      if (user[0]) {
        this.user = user[0];
        console.log(this.user);
        this.userImage = "../../../assets/person" + (this.user.Username.length % 9) + ".png";
        this.coverImage = "../../../assets/cover" + (this.user.Username.length % 4) + ".jfif";
        document.getElementById('coverDiv').style.backgroundImage = `url('${this.coverImage}')`;
        document.getElementById('coverDiv').style.backgroundRepeat = `no-repeat`;

        this.loggedUser = localStorage.getItem('username');
        // this.userService.getAllFriendships().subscribe((friendships: UserHasFriend[])=>{
        //   friendships.forEach(friendship=>{
        //     if(friendship.userId==this.user.id){
        //       this.following+=1;
        //     }
        //     if(friendship.friendId==this.user.id){
        //       this.followers+=1;
        //     }
        //   })
        // })
        // if (this.loggedUser != this.user.username) {
        //   //this.getUserFriendship(this.userId, localStorage.getItem('userId'));
        // }
      }
    })
  }

  getUserFriendship(userId, logedUserId) {
    this.userService.getFriendship(userId, logedUserId).subscribe((friendship: UserHasFriend[]) => {
      console.log(friendship)
      if (friendship.length!==0) {
        this.friendship = friendship[0];
        this.isFriends = true;
      }
    })
  }

  getUserResponses(userId) {
    this.userService.getAllResponses(userId).subscribe(events => {
      console.log(events);
    })
  }

  onRemoveFriend(e: Event) {

    this.userService.unfollowUser(localStorage.getItem('username'),this.user.Username).subscribe((res: UserHasFriend) => {
      console.log(res);
      if (res) {
        document.getElementById('followBtn').className = 'btn btn-info addBtn';
        document.getElementById('followBtn').innerHTML = "Follow";
        this.friendship = res;
        this.isFriends = true;
        this.followers+=1;
      }
    });
    // this.userService.unfollowUser(this.friendshipId).subscribe(res => {
    //   console.log(res);
    //   if (res) {
    //     document.getElementById('followBtn').className = 'btn btn-info addBtn';
    //     document.getElementById('followBtn').innerHTML = "Follow";
    //     this.friendshipId = null;
    //     this.friendship = null;
    //     this.isFriends = false;
    //     this.followers-=1;
    //   }
    // })
  }

  onAddFriend(e: Event) {
    this.userService.followUser(localStorage.getItem('username'),this.user.Username).subscribe((res: UserHasFriend) => {
      console.log(res);
      if (res) {
        document.getElementById('followBtn').className = 'btn btn-light addBtn';
        document.getElementById('followBtn').innerHTML = "Unfollow";
        this.friendship = res;
        this.isFriends = true;
        this.followers+=1;
      }
    });

  }

}