import { Component, OnInit, Input } from '@angular/core';
import { EventsService } from 'src/app/services/events.service';
import { Router } from '@angular/router';
import { Event } from 'src/app/models/Event';

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.scss']
})
export class EventsComponent implements OnInit {

  @Input() eventList: Event[] = [];

  constructor(private eventsService: EventsService) { }

  ngOnInit() {
      this.getEvents();
  }

  getEvents() {
    this.eventsService.getEvents().subscribe((res:Event[])=>{
      console.log(res);
      this.eventList=res;
    })
  }

}
