import { Component, OnInit, Input } from '@angular/core';
import { User } from 'src/app/models/User';
import { Router } from '@angular/router';

@Component({
  selector: 'app-people-list',
  templateUrl: './people-list.component.html',
  styleUrls: ['./people-list.component.scss']
})
export class PeopleListComponent implements OnInit {

  @Input() peopleList: User[];
  reducedPeopleList: User[] = [];
  otherPeople = 0;

  constructor(private router:Router) { }

  ngOnInit() {
    console.log(this.peopleList);
    if (this.peopleList.length > 11) {
      this.reducedPeopleList = this.peopleList.slice(0, 11);
      this.otherPeople = this.peopleList.length - 11;
    }
  }

  returnUsername(username) {
    return username.charAt(0).toUpperCase() + username.slice(1);
  }

  returnImage(id) {
    return ("../../../assets/person" + (id % 7) + ".png");
  }

  expandPeople() {
    this.reducedPeopleList = this.peopleList;
    this.otherPeople = 0;
  }

  gotoProfile(person:User){
    this.router.navigate(["/user", person.id]);
  }

}