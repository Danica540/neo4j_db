import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StarterComponent } from './components/starter/starter.component';
import { HomeComponent } from './components/home/home.component';
import { EventsComponent } from './components/events/events.component';
import { ProfileComponent } from './components/profile/profile.component';
import { EventDetailsComponent } from './components/event-details/event-details.component';
import { AddEventComponent } from './components/add-event/add-event.component';
import { FriendsComponent } from './components/friends/friends.component';


const routes: Routes = [
  {
    path: '',  pathMatch: 'full',component: StarterComponent
  },
  {
    path: "home",
    component: HomeComponent
  },
  {
    path: 'events',
    component: EventsComponent,
    pathMatch: 'full'
  },
  {
    path: "events/:eventId",
    component: EventDetailsComponent,
    pathMatch: 'full'
  },
  {
    path: "user/:userId",
    component: ProfileComponent,
    pathMatch: 'full'
  },
  {
    path:'addEvent',
    component:AddEventComponent
  },
  {
    path: "people",
    component: FriendsComponent,
    pathMatch: 'full'
  },
  {
    path: "profile",
    component: ProfileComponent,
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
