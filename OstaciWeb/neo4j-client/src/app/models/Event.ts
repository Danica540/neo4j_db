import { User } from './User';

/*
    public int id { get; set; }
    public string Title { get; set; }
    public string Description { get; set; }
    public string Time { get; set; }
    public string Place { get; set; }
    public string Adress { get; set; }
    public string City { get; set; }
    public string Type { get; set; }
    public FollowUser Creator { get; set; }
*/

export class Event {
    id?: number;
    title: string;
    description: string;
    date: string;
    address: string;
    city: string;
    place: string;
    listOfPeopleComing: any[];
    time: string;
    type: string;
    creator: User;

    constructor() {
        this.listOfPeopleComing = [];
    }
}

