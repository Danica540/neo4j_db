import { UserHasFriend } from './UserHasFriend';


/*
    public String Name { get; set; }
    public String Lastname { get; set; }
    public String Username { get; set; }
    public String Password { get; set; }
    public String Searchname { get; set; }
    public IList<FollowUser> Following { get; set; } = new List<FollowUser>();
    public IList<Event> Created { get; set; } = new List<Event>();
    public IList<Event> GoingTo { get; set; }=new List<Event>();
*/

export class User {
    id?: number;
    username: string;
    password: string;
    name: string;
    lastname: string;
    searchname: string;
    following: UserHasFriend[];
    created: Event[];
    goingTo: Event[];

    constructor() {
        this.created = [];
        this.following = [];
        this.goingTo = [];
    }

    setAllAttributes(usernameValue: string, passwordValue: string, name: string, lastname: string) {
        this.password = passwordValue;
        this.username = usernameValue;
        this.name = name;
        this.searchname = this.username.toLowerCase();
        this.lastname = lastname;
    }

    setAttributes(usernameValue: string, passwordValue: string) {
        this.password = passwordValue;
        this.username = usernameValue;
    }
}