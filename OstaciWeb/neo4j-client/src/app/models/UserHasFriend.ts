import { User } from './User';

export class UserHasFriend {
    id?: number;
    friendId: number;
    userId: number;
    friend?:User

    constructor(){
    }

    setAttributes(friendId:number,userId:number){
        this.userId=userId;
        this.friendId=friendId;
    }
}