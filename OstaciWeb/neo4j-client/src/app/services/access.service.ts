import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environmentVariables } from '../constants/url-constant';
import { User } from '../models/User';
const API_URL_NEO4J = environmentVariables.NEO4J_API_URL;

@Injectable({
  providedIn: 'root'
})
export class AccessService {

  constructor(private http: HttpClient) { }

  signUpUser(username: string, password: string, name: string, lastname: string) {
    let newUser = new User();
    newUser.setAllAttributes(username, password, name, lastname);
    return this.http.post(`${API_URL_NEO4J}/Register`, newUser);
  }

  signInUser(username: string, password: string) {
    let user = {
      "Username": username,
      "Password": password
    }
    return this.http.post(`${API_URL_NEO4J}/LogIn`, user);
  }
}
