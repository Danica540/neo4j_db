import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environmentVariables } from '../constants/url-constant';
import { Event } from '../models/Event';

const API_URL = environmentVariables.JSON_API_URL;

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  constructor(private http: HttpClient) { }

  getEvents() {
    return this.http.get(`${API_URL}/events`);
  }

  getEvent(id: number) {
    return this.http.get(`${API_URL}/events/${id}`);
  }

  updateEvent(event: Event) {
    return this.http.put(`${API_URL}/events/${event.id}`, event);
  }
}
