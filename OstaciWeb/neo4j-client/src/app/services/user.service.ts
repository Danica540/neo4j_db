import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environmentVariables } from '../constants/url-constant';
import { UserHasEvent } from '../models/UserHasEvent';

const API_URL_NEO4J = environmentVariables.NEO4J_API_URL;
const API_URL = environmentVariables.JSON_API_URL;

@Injectable({
  providedIn: 'root'
})

export class UserService {

  constructor(private http: HttpClient) { }

  getUser(username: string) {
    return this.http.get(`${API_URL_NEO4J}/User?ParcialUsername=${username.toLowerCase()}`);
  }

  followUser(username1:string,username2:string) {
    return this.http.get(`${API_URL_NEO4J}/Follow?Username1=${username1}&Username2=${username2}`);
  }

  unfollowUser(username1:string,username2:string) {
    return this.http.delete(`${API_URL_NEO4J}/Follow?Username1=${username1}&Username2=${username2}`);
  }

  getFriendship(userId, loggedUserId) {
    return this.http.get(`${API_URL}/friendships?userId=${loggedUserId}&friendId=${userId}`);
  }

  getAllFriendships() {
    return this.http.get(`${API_URL}/friendships`);
  }

  getResponse(userId: number, eventId: number) {
    return this.http.get(`${API_URL}/eventsInteresstedIn?userId=${userId}&eventId=${eventId}`);
  }

  getAllResponses(userId: number) {
    return this.http.get<UserHasEvent[]>(`${API_URL}/eventsInteresstedIn?userId=${userId}&_expand=event`);
  }

  addResponse(newUserResponse: UserHasEvent) {
    return this.http.post(`${API_URL}/eventsInteresstedIn`, newUserResponse);
  }

  deleteResponse(responseId: number) {
    return this.http.delete(`${API_URL}/eventsInteresstedIn/${responseId}`);
  }
}